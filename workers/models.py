# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth.models import User
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.conf import settings

# Create your models here.
@python_2_unicode_compatible
class Department(models.Model):
	title = models.CharField(default="Add New", max_length=50)
	def __str__(self):
		return self.title


@python_2_unicode_compatible
class Worker(models.Model):
    first_name = models.CharField(max_length=50, verbose_name="Имя")
    last_name = models.CharField(max_length=50, verbose_name="Фамилия")
    photo = models.ImageField(default = 'no_photo.png', verbose_name="Фото")
    alias = models.CharField(max_length=300, verbose_name="Алиасы")
    primary_alias = models.CharField(max_length=50, verbose_name="Главный Алиас", default="TBA")
    explonation = models.CharField(default="TBA", max_length=300, verbose_name="Почему так")
    department = models.ForeignKey(Department, verbose_name="Где работает")
    work = models.CharField(default="Пинает хуи", max_length=300, verbose_name="Что делает")

    def __str__(self):             
        return "%s %s" % (self.first_name, self.last_name)

