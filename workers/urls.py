from django.conf.urls import url
from . import views
from django.conf import settings
from django.views.static import serve
urlpatterns = [
    url(r'info/(?P<worker_id>[0-9]+)/$', views.worker_info, name = 'worker_info'),
    url(r'edit_worker/(?P<worker_id>[0-9]+)/$', views.edit_worker, name = 'edit_worker'),
    url(r'edit/(?P<worker_id>[0-9]+)/$', views.edit_form, name = 'edit_form'),
    url(r'add_worker/$', views.add_worker, name = 'add_worker'),
    url(r'validate_worker/$', views.validate_worker, name = 'validate_worker'),
    url(r'^$', views.main, name = 'main'),
]
