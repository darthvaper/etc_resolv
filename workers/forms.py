# -*- coding: utf-8 -*-
from django import forms
from models import Worker,Department

class AddWorker(forms.ModelForm):
	class Meta:
		model = Worker
		fields = '__all__'

	new_dep = forms.CharField(max_length=30, required=False, label = "Новый отдел")

	def __init__(self, *args, **kwargs):
		super(AddWorker, self).__init__(*args, **kwargs)
        # make `studio` not required, we'll check for one of `studio` or `new_studio` in the `clean` method
		self.fields['department'].required = False

	def clean(self):
		department = self.cleaned_data.get('department')
		new_dep = self.cleaned_data.get('new_dep')
		if not department and not new_dep:
            # neither was specified so raise an error to user
			raise forms.ValidationError('Must specify either department or New department!')
		elif not department:
            # get/create `Studio` from `new_studio` and use it for `studio` field
			department, created = Department.objects.get_or_create(title=new_dep)
			self.cleaned_data['department'] = department

		return super(AddWorker, self).clean()