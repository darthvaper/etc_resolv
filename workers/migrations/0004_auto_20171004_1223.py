# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-10-04 12:23
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('workers', '0003_auto_20171004_1219'),
    ]

    operations = [
        migrations.AlterField(
            model_name='worker',
            name='photo',
            field=models.ImageField(default='no_photo.png', upload_to=b''),
        ),
    ]
