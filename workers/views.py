# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render_to_response, redirect
from django.http import HttpResponse, JsonResponse
from .models import Department, Worker
from forms import AddWorker
from django.template.loader import render_to_string
from django.contrib.auth.decorators import login_required
#from django.core.context_processors import csrf

# Create your views here.
@login_required(login_url='/login/')
def main(request):
    add_form = AddWorker
    args = {}
    #args.update(csrf(request))
    args['workers'] = Worker.objects.all()
    args['form'] = add_form
    return render_to_response('index.html', args)

@login_required(login_url='/login/')
def add_worker(request):
    if request.POST:
        form = AddWorker(request.POST, request.FILES)
        if form.is_valid():
            worker = form.save(commit=False)
            form.save()

    return redirect('/')

@login_required(login_url='/login/')
def edit_form(request, worker_id):
    worker = Worker.objects.get(id=worker_id)
    initial_data = {
        'id ': worker_id,
        'first_name': worker.first_name,
        'last_name': worker.last_name,
        'photo': worker.photo,
        'alias': worker.alias,
        'primary_alias': worker.primary_alias,
        'explonation': worker.explonation,
        'department': worker.department,
        'work': worker.work,
    
    }

    #id = worker_id
    form = AddWorker(initial=initial_data)
    #html = render_to_string('index.html', {'form': form, 'id': worker_id})
    return HttpResponse(form.as_p())

@login_required(login_url='/login/')
def edit_worker(request, worker_id):
    if request.POST:
        #form = AddWorker(request.POST, request.FILES)
        worker = Worker.objects.get(pk=worker_id)
        new_dep = request.POST.get('department')
        if len(new_dep) == 0:
            new_dep_2 = request.POST.get('new_dep')
            department = Department.objects.get_or_create(title = new_dep_2)
            department = Department.objects.get(title = new_dep_2)
        else:
            department = Department.objects.get(id = new_dep)
        #print worker
        #if form.is_valid():
        worker.first_name = request.POST.get('first_name')
        worker.last_name = request.POST.get('last_name')
        worker.photo = request.FILES.get('photo')
        worker.alias = request.POST.get('alias')
        worker.primary_alias = request.POST.get('primary_alias')
        worker.explonation = request.POST.get('explonation')
        worker.department = department
        print worker.department
        worker.work = request.POST.get('work')
        worker.save()
        #print worker
        redirect_url = '/info/' + worker_id 
        return redirect(redirect_url)

def validate_worker(request):
    first_name = request.GET.get('worker_name')
    last_name = request.GET.get('worker_last_name')
    data = {
        'name_is_taken': Worker.objects.filter(first_name__iexact=first_name).exists(),
        'last_name_is_taken': Worker.objects.filter(last_name__iexact=last_name).exists()
    }
    return JsonResponse(data)


@login_required(login_url='/login/')
def worker_info(request, worker_id):
    single_worker = Worker.objects.get(id=worker_id)
    args = {}
    add_form = AddWorker
    args['id'] = worker_id
    args['form'] = add_form
    args['photo'] = single_worker.photo.url
    args['name'] = single_worker.first_name
    args['last_name'] = single_worker.last_name
    args['department'] = single_worker.department.title
    args['primary_alias'] = single_worker.primary_alias
    args['known_names'] = single_worker.alias
    args['job'] = single_worker.work
    args['reason'] = single_worker.explonation

   
    return render_to_response('worker_info.html', args)












